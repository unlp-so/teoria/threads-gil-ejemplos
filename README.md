# Threads una introducción pragmática

En este repositorio, dejamos ejemplos que son usados como complemento de una
[presentación sobre threads en diferentes
lenguajes](https://docs.google.com/presentation/d/1Y13gA31sIqmkVgAmMoasT-hrBJ7HpwT46naL2aVRPKw/edit?usp=sharing).
La idea es comparar de qué forma se comportan los threads que programamos desde
la perspectiva del SO.

## Requerimientos

Los ejemplos pueden correrse en Linux y también con Linux en Windows usando WSDL.
Las herramientas que usamos, por el momento contempla los siguientes lenguajes:

* **Java:** oracle 20
* **Ruby:** 
    * MRI >= 3
    * Jruby 9.4.2.0
* **Python:**
    * CPython >= 3.10
    * NoGIL >= 3.9.10

## Instalando las herramientas

Las pruebas pueden realizarse instalando manualmente los interpretes,
compiladores y runtimes, o mediante [asdf](https://asdf-vm.com/). Se provee un
archivo `.tool-versions` con las versiones usadas.

> TODO: agregar explicación de cómo usar docker y no instalar nada.

## Métricas con prometheus

Además de los fuentes, se provee del stack que realizará scrapping de métricas
de la PC donde se corran los ejemplos usando
[node-exporter](https://github.com/prometheus/node_exporter) y
[prometheus](https://prometheus.io/).

Para iniciar el stack:

```bash
cd prometheus/
docker-compose up -d
```

> Con `docker-compose ps` podemos observar el estado de los contenedores


Aguardamos termine de iniciar los contenedores y podemos acceder a un explorador
de prometehus que mostrará gráficas de uso de la CPU accediendo al siguiente
enlace:

a [Acceso a
  Prometheus](http://localhost:9090/graph?g0.expr=100%20-%20(avg%20by%20(instance%2Ccpu)%20(rate(node_cpu_seconds_total%7Bjob%3D%22node%22%2Cmode%3D%22idle%22%7D%5B1m%5D))%20*%20100)&g0.tab=0&g0.display_mode=lines&g0.show_exemplars=0&g0.range_input=5m&g1.expr=100%20-%20(avg%20by%20(instance)%20(rate(node_cpu_seconds_total%7Bjob%3D%22node%22%2Cmode%3D%22idle%22%7D%5B1m%5D))%20*%20100)&g1.tab=0&g1.display_mode=lines&g1.show_exemplars=0&g1.range_input=5m&g2.expr=100%20-%20(avg%20by%20(instance)%20(rate(node_cpu_seconds_total%7Bjob%3D%22node%22%2Cmode!%3D%22iowait%22%7D%5B1m%5D))%20*%20100)&g2.tab=0&g2.display_mode=lines&g2.show_exemplars=0&g2.range_input=5m)

